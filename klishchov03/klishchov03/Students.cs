﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace klishchov03
{
    class Students
    {
        /// <summary>
        /// Students class container
        /// </summary>
        private List<Student> StudentsList = new List<Student>();

        /// <summary>
        /// Validation error container
        /// </summary>
        private List<ValidationResult> Errors = new List<ValidationResult>();

        /// <summary>
        /// Adding new student
        /// </summary>
        /// <param name="student">Student class object</param>
        public void AddStudent(Student student)
        {
            var context = new ValidationContext(student);
            if (!Validator.TryValidateObject(student, context, Errors, true))
            {
                foreach (var error in Errors)
                {
                    Console.WriteLine(error.ErrorMessage);
                }
                Console.WriteLine("");
            }
            else
            {
                StudentsList.Add(student);
            }
        }

        /// <summary>
        /// Printing information about all students
        /// </summary>
        public void ShowAllStudents()
        {
            StudentsList.ForEach(delegate (Student student)
            {
                student.StudInfo();
                Console.WriteLine("");
            });
        }

        /// <summary>
        /// Searching students by last name
        /// </summary>
        /// <param name="lastName">Last name of student</param>
        public void SearchByLastName(string lastName)
        {
            StudentsList.ForEach(delegate (Student student)
            {
                if(student.LastName == lastName)
                {
                    student.StudInfo();
                    Console.WriteLine("");
                }
            });
        }

        /// <summary>
        /// Searching students by birth date
        /// </summary>
        /// <param name="birthDate">Date of birth</param>
        public void SearchByBirthDate(DateTime birthDate)
        {
            StudentsList.ForEach(delegate (Student student)
            {
                if (student.BirthDate == birthDate)
                {
                    student.StudInfo();
                    Console.WriteLine("");
                }
            });
        }

        /// <summary>
        /// Searching students by admission date
        /// </summary>
        /// <param name="admissionDate">Date of admission of student</param>
        public void SearchByAdmissionDate(DateTime admissionDate)
        {
            StudentsList.ForEach(delegate (Student student)
            {
                if (student.AdmissionDate == admissionDate)
                {
                    student.StudInfo();
                    Console.WriteLine("");
                }
            });
        }


        /// <summary>
        /// Saving the data of students in txt file
        /// </summary>
        public void SaveInTXT()
        {
            string[] lines = new string[StudentsList.Count];
            int i = 0;
            StudentsList.ForEach(delegate (Student student)
            {
                lines.SetValue(student.ToString(), i);
                i++;
            });
            System.IO.File.WriteAllLines(@"C:\.NET labs\klishchov03\klishchov03\Students.txt", lines);
        }

        /// <summary>
        /// Restore the data of students from txt file
        /// </summary>
        public void RestoreFromTXT()
        {
            StudentsList.Clear();
            string[] lines = System.IO.File.ReadAllLines(@"C:\.NET labs\klishchov03\klishchov03\Students.txt");
            foreach (string line in lines)
            {
                AddStudent(new Student(
                    line.Split("|").GetValue(0).ToString(),
                    line.Split("|").GetValue(1).ToString(),
                    line.Split("|").GetValue(2).ToString(),
                    line.Split("|").GetValue(3).ToString(),
                    line.Split("|").GetValue(4).ToString(),
                    line.Split("|").GetValue(5).ToString(),
                    line.Split("|").GetValue(6).ToString(),
                    DateTime.ParseExact(line.Split("|").GetValue(7).ToString(), "dd/MM/yyyy", null),
                    DateTime.ParseExact(line.Split("|").GetValue(8).ToString(), "dd/MM/yyyy", null)));
            }
        }

        /// <summary>
        /// Removing the data of students by index
        /// </summary>
        /// <param name="idx">Index of student</param>
        public void RemoveStudData(int idx)
        {
            StudentsList.RemoveAt(idx);
        }

        /// <summary>
        /// Replacing the data of student by index
        /// </summary>
        /// <param name="idx">Index of student</param>
        /// <param name="newStud">New student`s data</param>
        public void ReplaceStudData(int idx, Student newStud)
        {
            StudentsList.RemoveAt(idx);
            StudentsList.Insert(idx, newStud);
        }
    }
}
