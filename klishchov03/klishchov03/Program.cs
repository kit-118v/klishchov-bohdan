﻿using System;

namespace klishchov03
{
    class Program
    {
        static void Main(string[] args)
        {
            Students students = new Students();
            students.AddStudent(new Student(Guid.NewGuid().ToString(), "Bohdan", "Klishchov", "Romanovich", "CIT", "Computer Engineering", "B", new DateTime(2000, 9, 4), new DateTime(2018, 9, 1)));
            students.AddStudent(new Student(Guid.NewGuid().ToString(), "Anton", "Ivanov", "Ihorevich", "CIT", "Networks", "A", new DateTime(2001, 9, 4), new DateTime(2019, 9, 3)));
            students.AddStudent(new Student(Guid.NewGuid().ToString(), "Dmitry", "Nadtoka", "Dmitrovich", "MIT", "Engineering", "C", new DateTime(2001, 9, 4), new DateTime(2019, 9, 1)));
            students.AddStudent(new Student(Guid.NewGuid().ToString(), "Nikita", "Antonov", "Andreevich", "CIT", "Engineering", "B", new DateTime(2000, 9, 4), new DateTime(2018, 8, 29)));
            students.AddStudent(new Student(Guid.NewGuid().ToString(), "Oleh", "Olehov", "Olehovich", "CIT", "Engineering", "E", new DateTime(2002, 9, 4), new DateTime(2020, 10, 12)));

            students.SaveInTXT();
            students.ShowAllStudents();
            Console.WriteLine("-------------------------------------");
            students.RestoreFromTXT();
            students.ShowAllStudents();
            Console.WriteLine("-------------------------------------");
            students.RemoveStudData(3);
            students.ShowAllStudents();
            Console.WriteLine("-------------------------------------");
            students.ReplaceStudData(3,
                new Student(Guid.NewGuid().ToString(), "Oleh", "Olehov", "Olehovich", "MIT", "Engineering", "E",
                new DateTime(2002, 9, 4), new DateTime(2020, 10, 12)));
            students.ShowAllStudents();
        }
    }
}
