﻿using System;
using System.ComponentModel.DataAnnotations;


namespace klishchov03
{
    /// <summary>
    /// Student class
    /// </summary>
    class Student
    {
        public string ID { get; private set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string FirstName { get; private set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string LastName { get; private set; }

        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string MiddleName { get; private set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Faculty { get; private set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Specialty { get; private set; }

        [Required]
        [StringLength(3, MinimumLength = 1)]
        public string GroupIndex { get; private set; }
        public DateTime BirthDate { get; private set; }
        public DateTime AdmissionDate { get; private set; }

        public Student(string id, string firstName, string lastName, string middleName, string faculty, string specialty, string groupIndex, DateTime birthDate, DateTime admissionDate)
        {
            ID = id;
            FirstName = firstName;
            LastName = lastName;
            MiddleName = middleName;
            Faculty = faculty;
            Specialty = specialty;
            GroupIndex = groupIndex;
            BirthDate = birthDate;
            AdmissionDate = admissionDate;

        }

        /// <summary>
        /// Getting full name of student
        /// </summary>
        /// <returns>String with full name of student</returns>
        public string GetFullName()
        {
            return $"{FirstName} {LastName} {MiddleName}";
        }

        public override string ToString()
        {
            return $"{ID}|{FirstName}|{LastName}|{MiddleName}|{Faculty}|{Specialty}|{GroupIndex}|{BirthDate.ToString("dd/MM/yyyy")}|{AdmissionDate.ToString("dd/MM/yyyy")}";
        }

        /// <summary>
        /// Printing information about student
        /// </summary>
        public void StudInfo()
        {
            Console.WriteLine($"Student ID: {ID}");
            Console.WriteLine($"Full name: {GetFullName()}");
            Console.WriteLine($"Faculty: {Faculty}");
            Console.WriteLine($"Specialty: {Specialty}");
            Console.WriteLine($"Index of group: {GroupIndex}");
            Console.WriteLine($"Date of Birth: {BirthDate.ToString("dd/MM/yyyy")}");
            Console.WriteLine($"Date of admission: {AdmissionDate.ToString("dd/MM/yyyy")}");
        }
    }
}
