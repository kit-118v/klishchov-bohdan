﻿using ConsoleTables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace klishchov06
{
    class Students
    {
        /// <summary>
        /// Students class container
        /// </summary>
        private List<Student> StudentsList = new List<Student>();

        /// <summary>
        /// Validation error container
        /// </summary>
        private List<ValidationResult> Errors = new List<ValidationResult>();

        /// <summary>
        /// Adding new student
        /// </summary>
        /// <param name="student">Student class object</param>
        public void AddStudent(Student student)
        {
            var context = new ValidationContext(student);
            if (!Validator.TryValidateObject(student, context, Errors, true))
            {
                foreach (var error in Errors)
                {
                    Console.WriteLine(error.ErrorMessage);
                }
                Console.WriteLine();
            }
            else
            {
                StudentsList.Add(student);
            }
        }

        /// <summary>
        /// Printing information about all students
        /// </summary>
        public void ShowAllStudents()
        {
            StudentsList.ForEach(delegate (Student student)
            {
                student.StudInfo();
                Console.WriteLine();
            });
        }

        /// <summary>
        /// Searching students by last name
        /// </summary>
        /// <param name="lastName">Last name of student</param>
        public void SearchByLastName(string lastName)
        {
            StudentsList.ForEach(delegate (Student student)
            {
                if(student.LastName == lastName)
                {
                    student.StudInfo();
                    Console.WriteLine("");
                }
            });
        }

        /// <summary>
        /// Searching students by birth date
        /// </summary>
        /// <param name="birthDate">Date of birth</param>
        public void SearchByBirthDate(DateTime birthDate)
        {
            StudentsList.ForEach(delegate (Student student)
            {
                if (student.BirthDate == birthDate)
                {
                    student.StudInfo();
                    Console.WriteLine("");
                }
            });
        }

        /// <summary>
        /// Searching students by admission date
        /// </summary>
        /// <param name="admissionDate">Date of admission of student</param>
        public void SearchByAdmissionDate(DateTime admissionDate)
        {
            StudentsList.ForEach(delegate (Student student)
            {
                if (student.AdmissionDate == admissionDate)
                {
                    student.StudInfo();
                    Console.WriteLine();
                }
            });
        }


        /// <summary>
        /// Saving the data of students in txt file
        /// </summary>
        public void SaveInTXT()
        {
            string[] lines = new string[StudentsList.Count];
            int i = 0;
            StudentsList.ForEach(delegate (Student student)
            {
                lines.SetValue(student.ToString(), i);
                i++;
            });
            System.IO.File.WriteAllLines(@"C:\.NET labs\klishchov03\klishchov03\Students.txt", lines);
        }

        /// <summary>
        /// Restore the data of students from txt file
        /// </summary>
        public void RestoreFromTXT()
        {
            StudentsList.Clear();
            string[] lines = System.IO.File.ReadAllLines(@"C:\.NET labs\klishchov03\klishchov03\Students.txt");
            foreach (string line in lines)
            {
                AddStudent(new Student(
                    line.Split("|").GetValue(0).ToString(),
                    line.Split("|").GetValue(1).ToString(),
                    line.Split("|").GetValue(2).ToString(),
                    line.Split("|").GetValue(3).ToString(),
                    line.Split("|").GetValue(4).ToString(),
                    line.Split("|").GetValue(5).ToString(),
                    line.Split("|").GetValue(6).ToString(),
                    DateTime.ParseExact(line.Split("|").GetValue(7).ToString(), "dd/MM/yyyy", null),
                    DateTime.ParseExact(line.Split("|").GetValue(8).ToString(), "dd/MM/yyyy", null)));
            }
        }

        /// <summary>
        /// Removing the data of students by index
        /// </summary>
        /// <param name="idx">Index of student</param>
        public void RemoveStudData(int idx)
        {
            StudentsList.RemoveAt(idx);
        }

        /// <summary>
        /// Replacing the data of student by index
        /// </summary>
        /// <param name="idx">Index of student</param>
        /// <param name="newStud">New student`s data</param>
        public void ReplaceStudData(int idx, Student newStud)
        {
            StudentsList.RemoveAt(idx);
            StudentsList.Insert(idx, newStud);
        }

        /// <summary>
        /// Getting a group of student
        /// </summary>
        /// <param name="idx">Index in container</param>
        /// <returns>Group of student</returns>
        public string GetStudentGroupInfo(int idx)
        {
            var cQuery =
            from c in StudentsList[idx].Faculty
            where char.IsUpper(c)
            select c;

            StringBuilder result = new StringBuilder();
            foreach (char c in cQuery)
            {
                    result.Append(c);
            }
            result.Append(StudentsList[idx].Specialty);
            string year = StudentsList[idx].AdmissionDate.Year.ToString();
            result.Append('.' + year.Remove(0, 2) + StudentsList[idx].GroupIndex);
            return result.ToString();
        }

        /// <summary>
        /// Getting course of student
        /// </summary>
        /// <param name="idx">Index in container</param>
        /// <returns>Course of student</returns>
        public string GetStudentCourse(int idx)
        {
            StringBuilder result = new StringBuilder();
            result.Append(DateTime.Now.Subtract(StudentsList[idx].AdmissionDate).Days / 365 + 1);
            return $"Course: {result.ToString()}";
        }

        /// <summary>
        /// Getting student life length in days
        /// </summary>
        /// <param name="idx">Index in container</param>
        /// <returns>Life length of student in days</returns>
        public string GetStudentLifeLengthInDays(int idx)
        {
            StringBuilder result = new StringBuilder();
            result.Append(DateTime.Now.Subtract(StudentsList[idx].BirthDate).Days );
            return result.ToString();
        }

        /// <summary>
        /// Printing table with students info
        /// </summary>
        public void ShowStudentsInfoInTable()
        {
            var table = new ConsoleTable("ID", "Full name", "Fuculty", "Spesiality", "Group index", "Birth date", "Admission date");
            StudentsList.ForEach(delegate (Student student)
            {
                table.AddRow(student.ID, student.GetFullName(), student.Faculty, student.Specialty, student.GroupIndex, student.BirthDate.ToString("dd/MM/yyyy"), student.AdmissionDate.ToString("dd/MM/yyyy"));
            });
            table.Write();
        }

        /// <summary>
        /// Serialize students list 
        /// </summary>
        public void SerializeStudents()
        {
            System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(List<Student>));
            var path = "C:\\.NET labs\\klishchov05\\Students.xml";
            System.IO.FileStream file = System.IO.File.Create(path);
            writer.Serialize(file, StudentsList);
            file.Close();
        }

        /// <summary>
        /// Deserialize students list 
        /// </summary>
        public void DeserializeStudents()
        {
            System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(List<Student>));
            var path = "C:\\.NET labs\\klishchov05\\Students.xml";
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            StudentsList = (List<Student>)reader.Deserialize(file);
            file.Close();
        }

        /// <summary>
        /// Delete of group students
        /// </summary>
        /// <param name="groupIndex">Index of group</param>
        public void DeleteStudentsByGroup(string groupIndex)
        {
            for (int i = 0; i < StudentsList.Count; i++)
            {
                if (StudentsList[i].GroupIndex == groupIndex)
                {
                    RemoveStudData(i);
                }
            }
        }

        /// <summary>
        /// Culculating average age of students of fuculty
        /// </summary>
        /// <param name="fuculty">Fuculty of students</param>
        public void CulcAverageAgeStudOfFuculty(string fuculty)
        {
            int totalAge = 0;
            int countOfStud = 0;
            StudentsList.ForEach(delegate (Student student)
            {
                if (student.Faculty == fuculty)
                {
                    totalAge = totalAge + DateTime.Now.Subtract(student.BirthDate).Days / 365;
                    countOfStud++;
                }
            });
            Console.WriteLine("Average age: " + totalAge/countOfStud);
        }

        /// <summary>
        /// Testing serialization and deserialization
        /// </summary>
        public void TestSerialization()
        {
            SerializeStudents();
            System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(List<Student>));
            var path = "C:\\.NET labs\\klishchov05\\Students.xml";
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            var newStudentsList = (List<Student>)reader.Deserialize(file);
            int succsess = 0;
            for (int i = 0; i < StudentsList.Count; i++)
            {
                if (StudentsList[i].ID == newStudentsList[i].ID)
                {
                    succsess++;
                }
            }
            if (succsess == StudentsList.Count)
            {
                Console.WriteLine("Serialization succsessfull");
            } else
            {
                Console.WriteLine("Serialization Error");
            }
            file.Close();
        }
    }
}
