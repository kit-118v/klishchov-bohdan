﻿using System;
using System.ComponentModel.DataAnnotations;


namespace klishchov05
{
    /// <summary>
    /// Student class
    /// </summary>
    public class Student
    {
        public string ID { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string LastName { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string MiddleName { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Faculty { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Specialty { get; set; }

        [Required]
        [StringLength(3, MinimumLength = 1)]
        public string GroupIndex { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime AdmissionDate { get; set; }

        public Student(string id, string firstName, string lastName, string middleName, string faculty, string specialty, string groupIndex, DateTime birthDate, DateTime admissionDate)
        {
            ID = id;
            FirstName = firstName;
            LastName = lastName;
            MiddleName = middleName;
            Faculty = faculty;
            Specialty = specialty;
            GroupIndex = groupIndex;
            BirthDate = birthDate;
            AdmissionDate = admissionDate;

        }

        public Student()
        {

        }

        /// <summary>
        /// Getting full name of student
        /// </summary>
        /// <returns>String with full name of student</returns>
        public string GetFullName()
        {
            return $"{FirstName} {LastName} {MiddleName}";
        }

        public override string ToString()
        {
            return $"{ID}|{FirstName}|{LastName}|{MiddleName}|{Faculty}|{Specialty}|{GroupIndex}|{BirthDate.ToString("dd/MM/yyyy")}|{AdmissionDate.ToString("dd/MM/yyyy")}";
        }

        /// <summary>
        /// Printing information about student
        /// </summary>
        public void StudInfo()
        {
            Console.WriteLine($"Student ID: {ID}");
            Console.WriteLine($"Full name: {GetFullName()}");
            Console.WriteLine($"Faculty: {Faculty}");
            Console.WriteLine($"Specialty: {Specialty}");
            Console.WriteLine($"Index of group: {GroupIndex}");
            Console.WriteLine($"Date of Birth: {BirthDate.ToString("dd/MM/yyyy")}");
            Console.WriteLine($"Date of admission: {AdmissionDate.ToString("dd/MM/yyyy")}");
        }
    }
}
