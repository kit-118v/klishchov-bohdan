﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace klishchov02
{
    class Students
    {
        /// <summary>
        /// Students class container
        /// </summary>
        private List<Student> StudentsList = new List<Student>();

        /// <summary>
        /// Validation error container
        /// </summary>
        private List<ValidationResult> Errors = new List<ValidationResult>();

        /// <summary>
        /// Adding new student
        /// </summary>
        /// <param name="student">Student class object</param>
        public void AddStudent(Student student)
        {
            var context = new ValidationContext(student);
            if (!Validator.TryValidateObject(student, context, Errors, true))
            {
                foreach (var error in Errors)
                {
                    Console.WriteLine(error.ErrorMessage);
                }
                Console.WriteLine("");
            }
            else
            {
                StudentsList.Add(student);
            }
        }

        /// <summary>
        /// Printing information about all students
        /// </summary>
        public void ShowAllStudents()
        {
            StudentsList.ForEach(delegate (Student student)
            {
                student.StudInfo();
                Console.WriteLine("");
            });
        }

        /// <summary>
        /// Searching students by last name
        /// </summary>
        /// <param name="lastName">Last name of student</param>
        public void SearchByLastName(string lastName)
        {
            StudentsList.ForEach(delegate (Student student)
            {
                if(student.LastName == lastName)
                {
                    student.StudInfo();
                    Console.WriteLine("");
                }
            });
        }

        /// <summary>
        /// Searching students by birth date
        /// </summary>
        /// <param name="birthDate">Date of birth</param>
        public void SearchByBirthDate(DateTime birthDate)
        {
            StudentsList.ForEach(delegate (Student student)
            {
                if (student.BirthDate == birthDate)
                {
                    student.StudInfo();
                    Console.WriteLine("");
                }
            });
        }

        /// <summary>
        /// Searching students by admission date
        /// </summary>
        /// <param name="admissionDate">Date of admission of student</param>
        public void SearchByAdmissionDate(DateTime admissionDate)
        {
            StudentsList.ForEach(delegate (Student student)
            {
                if (student.AdmissionDate == admissionDate)
                {
                    student.StudInfo();
                    Console.WriteLine("");
                }
            });
        }
    }
}
