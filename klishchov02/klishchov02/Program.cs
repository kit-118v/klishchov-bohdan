﻿using System;

namespace klishchov02
{
    class Program
    {
        static void Main(string[] args)
        {
            Students students = new Students();
            students.AddStudent(new Student("Bohdan", "Klishchov", "Romanovich", "CIT", "Computer Engineering", "B", new DateTime(2000, 9, 4), new DateTime(2018, 9, 1)));
            students.AddStudent(new Student("Anton", "Ivanov", "Ihorevich", "CIT", "Computer Networks", "A", new DateTime(2001, 9, 4), new DateTime(2019, 9, 3)));
            students.AddStudent(new Student("Dmitry", "Nadtoka", "Dmitrovich", "MIT", "Mechanical Engineering", "C", new DateTime(2001, 9, 4), new DateTime(2019, 9, 1)));
            students.AddStudent(new Student("Nikita", "Antonov", "Andreevich", "CIT", "Computer Engineering", "B", new DateTime(2000, 9, 4), new DateTime(2018, 8, 29)));
            students.AddStudent(new Student("Oleh", "Olehov", "Olehovich", "CIT", "Electrical Engineering", "E", new DateTime(2002, 9, 4), new DateTime(2020, 10, 12)));

            Console.WriteLine("Search by last name:\n");
            students.SearchByLastName("Antonov");
            Console.WriteLine("---------------------------------------------------------------");
            Console.WriteLine("Search by birth date:\n");
            students.SearchByBirthDate(new DateTime(2000, 9, 4));
            Console.WriteLine("---------------------------------------------------------------");
            Console.WriteLine("Search by admission date:\n");
            students.SearchByAdmissionDate(new DateTime(2020, 10, 12));
        }
    }
}
