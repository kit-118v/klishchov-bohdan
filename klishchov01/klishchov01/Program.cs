﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace klishchov01
{
    /// <summary>
    /// program class
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            var errors = new List<ValidationResult>();

            var students = new List<Student>();
            students.Add(new Student("Bohdan", "Klishchov", "Romanovich", "CIT", "Computer Engineering", "B", new DateTime(2000, 9, 4), new DateTime(2018, 9, 1)));
            students.Add(new Student("Anton", "Ivanov", "Ihorevich", "CIT", "Computer Networks", "A", new DateTime(2001, 9, 4), new DateTime(2019, 9, 3)));
            students.Add(new Student("Dmitry", "Nadtoka", "Dmitrovich", "MIT", "Mechanical Engineering", "C", new DateTime(2001, 9, 4), new DateTime(2019, 9, 1)));
            students.Add(new Student("Nikita", "Antonov", "Andreevich", "CIT", "Computer Engineering", "B", new DateTime(2000, 9, 4), new DateTime(2018, 8, 29)));
            students.Add(new Student("Oleh", "Olehov", "Olehovich", "CIT", "Electrical Engineering", "E", new DateTime(2002, 9, 4), new DateTime(2020, 10, 12)));

            students.ForEach(delegate (Student student) 
            {
                var context = new ValidationContext(student);
                if (!Validator.TryValidateObject(student, context, errors, true))
                {
                    Console.WriteLine($"Student ID {student.ID}");
                    foreach (var error in errors)
                    {
                        Console.WriteLine(error.ErrorMessage);
                    }
                    Console.WriteLine("\n");
                }
                else
                {
                    student.StudInfo();
                    Console.WriteLine("\n");
                }
            });
        }
    }
}
