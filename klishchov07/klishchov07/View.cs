﻿using System;

namespace klishchov07
{
    static class View
    {
		/// <summary>
		/// Showing user interface in terminal
		/// </summary>
        public static void ShowUserInterface()
        {
            Students students = new Students();
			int choose = 0;


			do
			{
				Console.WriteLine("1 - Add new student");
				Console.WriteLine("2 - Show all students");
				Console.WriteLine("3 - Search by last name");
				Console.WriteLine("4 - Remove data of student");
				Console.WriteLine("5 - Show students list in table");
				Console.WriteLine("6 - Serialize");
				Console.WriteLine("7 - Deserialize");
				Console.WriteLine("8 - Culculate average age of students of fuculty");
				Console.WriteLine("9 - Add five students");
				Console.WriteLine("0 - Exit");

				while (true)
				{
					Console.WriteLine("Your choose: ");
					choose = Convert.ToInt16(Console.ReadLine());
					if (choose < 0 || choose > 9)
					{
						Console.WriteLine("Invalid number of operation");
						continue;
					}
					break;
				}
				Console.WriteLine();

				switch (choose)
				{
					case 1:
						Console.WriteLine("Enter first name: ");
						var firstName = Console.ReadLine();
						Console.WriteLine("Enter last name: ");
						var lastName = Console.ReadLine();
						Console.WriteLine("Enter middle name: ");
						var middleName = Console.ReadLine();
						Console.WriteLine("Enter fuculty: ");
						var fuculty = Console.ReadLine();
						Console.WriteLine("Enter index of speciality (123, 43, 111...): ");
						var speciality = Console.ReadLine();
						Console.WriteLine("Enter group index (A, B, C, AI...): ");
						var group = Console.ReadLine();
						Console.WriteLine("Enter birth date (e.g. 10/22/1987): ");
						DateTime birthDate = DateTime.Parse(Console.ReadLine());
						Console.WriteLine("Enter admission date (e.g. 10/1/2020): ");
						DateTime admissionDate = DateTime.Parse(Console.ReadLine());
						students.AddStudent(new Student(Guid.NewGuid().ToString(), firstName, lastName, middleName, fuculty, speciality, group, birthDate, admissionDate));
						Console.WriteLine();
						break;
					case 2:
						students.ShowAllStudents();
						break;
					case 3:
						Console.WriteLine("Enter last name: ");
						lastName = Console.ReadLine();
						students.SearchByLastName(lastName);
						Console.WriteLine();
						break;
					case 4:
						students.PrintAllNames();
                        while (true)
                        {
							Console.WriteLine("Enter index of student: ");
							var index = Convert.ToInt16(Console.ReadLine());
							if (index < 1 || index > students.GetStudentsCount())
							{
								Console.WriteLine("Invalid index");
								continue;
							}
							else
							{
								students.RemoveStudData(index);
								break;
							}

						}
						break;
					case 5:
						students.ShowStudentsInfoInTable();
						break;
					case 6:
						students.SerializeStudents();
						break;
					case 7:
						students.DeserializeStudents();
						break;
					case 8:
						Console.WriteLine("Enter fuculty: ");
						fuculty = Console.ReadLine();
						students.CulcAverageAgeStudOfFuculty(fuculty);
						Console.WriteLine();
						break;
					case 9:
						students.AddStudent(new Student(Guid.NewGuid().ToString(), "Bohdan", "Klishchov", "Romanovich", "CIT", "123", "B", new DateTime(2000, 9, 4), new DateTime(2018, 9, 1)));
						students.AddStudent(new Student(Guid.NewGuid().ToString(), "Anton", "Ivanov", "Ihorevich", "CIT", "124", "A", new DateTime(2001, 9, 4), new DateTime(2019, 9, 3)));
						students.AddStudent(new Student(Guid.NewGuid().ToString(), "Dmitry", "Nadtoka", "Dmitrovich", "MIT", "127", "C", new DateTime(2001, 9, 4), new DateTime(2019, 9, 1)));
						students.AddStudent(new Student(Guid.NewGuid().ToString(), "Nikita", "Antonov", "Andreevich", "CIT", "112", "B", new DateTime(2000, 9, 4), new DateTime(2018, 8, 29)));
						students.AddStudent(new Student(Guid.NewGuid().ToString(), "Oleh", "Olehov", "Olehovich", "CIT", "111", "E", new DateTime(2002, 9, 4), new DateTime(2020, 10, 12)));
						break;
					case 0:
						return;
				}
			} while (true);
		}
    }
}
