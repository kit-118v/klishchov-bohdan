﻿using System;
using System.Linq;

namespace klishchov04
{
    class Program
    {
        static void Main(string[] args)
        {
            Students students = new Students();
            students.AddStudent(new Student(Guid.NewGuid().ToString(), "Bohdan", "Klishchov", "Romanovich", "CIT", "123", "B", new DateTime(2000, 9, 4), new DateTime(2018, 9, 1)));
            students.AddStudent(new Student(Guid.NewGuid().ToString(), "Anton", "Ivanov", "Ihorevich", "CIT", "124", "A", new DateTime(2001, 9, 4), new DateTime(2019, 9, 3)));
            students.AddStudent(new Student(Guid.NewGuid().ToString(), "Dmitry", "Nadtoka", "Dmitrovich", "MIT", "127", "C", new DateTime(2001, 9, 4), new DateTime(2019, 9, 1)));
            students.AddStudent(new Student(Guid.NewGuid().ToString(), "Nikita", "Antonov", "Andreevich", "CIT", "112", "B", new DateTime(2000, 9, 4), new DateTime(2018, 8, 29)));
            students.AddStudent(new Student(Guid.NewGuid().ToString(), "Oleh", "Olehov", "Olehovich", "CIT", "111", "E", new DateTime(2002, 9, 4), new DateTime(2020, 10, 12)));

            students.SearchByLastName("Klishchov");
            Console.WriteLine("Group of student: " + students.GetStudentGroupInfo(0));
            Console.WriteLine("Course of student: " + students.GetStudentCourse(0));
            Console.WriteLine("Life length of student: " + students.GetStudentLifeLengthInDays(0));
        }
    }
}
